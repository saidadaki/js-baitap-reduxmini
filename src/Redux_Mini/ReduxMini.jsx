import React, { Component } from "react";
import { connect } from "react-redux";

class ReduxMini extends Component {
  render() {
    return (
      <div>
        <nav className="h1 bg-dark text-light mx-auto py-3">
          Number Redux JS
        </nav>
        <button onClick={this.props.handleGiam} className="btn btn-danger mx-3">
          -
        </button>
        <strong className="display-1 mx-3">{this.props.number}</strong>
        <button
          onClick={this.props.handleTang}
          className="btn btn-success mx-3"
        >
          +
        </button>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    number: state.soLuong.number,
  };
};
let mapDispatchToProps = (dispatch) => {
  return {
    handleTang: () => {
      let action = {
        type: "TANG_SO_LUONG",
        payload: 1,
      };
      dispatch(action);
    },
    handleGiam: () => {
      let action = {
        type: "GIAM_SO_LUONG",
        payload: 1,
      };
      dispatch(action);
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(ReduxMini);

import logo from './logo.svg';
import './App.css';
import ReduxMini from './Redux_Mini/ReduxMini';

function App() {
  return (
    <div className="App">
      <ReduxMini />
    </div>
  );
}

export default App;
